import React, { Component } from "react";

class App extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }
  componentDidMount() {
    let requestPath = "http://demo.is-a-chef.com/common_includes/test.json";
    fetch(requestPath)
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ data: data });
      })
      .catch(err => {
        // Do something for an error here
      });
  }

  render() {
    return (
      <div className="container-fluid">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Start</th>
              <th scope="col">End</th>
              <th scope="col">Barber</th>
              <th scope="col">Services</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map(item => (
              <tr key={item.last + item.first}>
                <td key={item.first + item.start}>{item.first}</td>
                <td key={item.first + item.end}>{item.start}</td>
                <td key={item.first + item.barber}>{item.end}</td>
                <td key={item.first + item.last}>{item.barber}</td>
                <td>Services</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
